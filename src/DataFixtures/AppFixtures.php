<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Genre;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $genreRoman = $this->createGenre('Roman');
        $authorHugo = $this->createAuthor(
            'Victor Hugo',
            \DateTime::createFromFormat('Y-m-d', '1802-02-26'),
            'Besançon',
            \DateTime::createFromFormat('Y-m-d', '1885-05-22'),
            'Paris',
        );
        $bookMiserable = $this->createBook(
            "Les Miséables",
            DateTime::createFromFormat("Y-m-d", "1862-01-01"),
            $authorHugo,
            $genreRoman
        );

        $genreFantasy = $this->createGenre('Fantasy');
        $authorMartin = $this->createAuthor(
            'George R. R. Martin',
            \DateTime::createFromFormat('Y-m-d', '1948-09-20'),
            'Bayonne, New Jersey, États-Unis',
            null,
            null,
        );
        $bookFeu = $this->createBook(
            "Feu et Sang",
            DateTime::createFromFormat("Y-m-d", "2018-11-20"),
            $authorMartin,
            $genreFantasy
        );

        $manager->flush();
    }

    protected function createAuthor(
        string $name,
        \DateTime $brithDate = null,
        string $brithPlace = null,
        \DateTime $deathDate = null,
        string $deathPlace = null
    ) {
        $author = new Author();
        $author->setName($name);
        $author->setBirthDate($brithDate);
        $author->setBirthPlace($brithPlace);
        $author->setDeathDate($deathDate);
        $author->setDeathPlace($deathPlace);

        $this->manager->persist($author);

        return $author;
    }

    protected function createGenre(string $name)
    {
        $genre = new Genre();
        $genre->setName($name);

        $this->manager->persist($genre);

        return $genre;
    }

    protected function createBook(string $name, DateTime $publicationDate, Author $author, Genre $genre) 
    {
        $book = new Book();
        $book->setName($name);
        $book->setPublicationDate($publicationDate);
        $book->setAuthor($author);
        $book->setGenre($genre);

        $this->manager->persist($book);
        
        return $genre;
    }
}
