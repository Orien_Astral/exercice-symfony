<?php

namespace App\Entity;

use App\Repository\AuthorRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 */
#[ApiResource(
    normalizationContext: ['groups' => ['read:collection', 'read:item']]
)]
class Author
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups(['read:collection', 'read:item'])]
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(['read:collection', 'read:item'])]
    private $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    #[Groups(['read:collection', 'read:item'])]
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups(['read:collection', 'read:item'])]
    private $birthPlace;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    #[Groups(['read:collection', 'read:item'])]
    private $deathDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups(['read:collection', 'read:item'])]
    private $deathPlace;

    /**
     * @ORM\OneToMany(targetEntity=Book::class, mappedBy="author")
     */
    #[Groups(['read:collection', 'read:item'])]
    private $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getBirthPlace(): ?string
    {
        return $this->birthPlace;
    }

    public function setBirthPlace(?string $birthPlace): self
    {
        $this->birthPlace = $birthPlace;

        return $this;
    }

    public function getDeathDate(): ?\DateTimeInterface
    {
        return $this->deathDate;
    }

    public function setDeathDate(?\DateTimeInterface $deathDate): self
    {
        $this->deathDate = $deathDate;

        return $this;
    }

    public function getDeathPlace(): ?string
    {
        return $this->deathPlace;
    }

    public function setDeathPlace(?string $deathPlace): self
    {
        $this->deathPlace = $deathPlace;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->setAuthor($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            // set the owning side to null (unless already changed)
            if ($book->getAuthor() === $this) {
                $book->setAuthor(null);
            }
        }

        return $this;
    }

    #[Groups(['read:collection', 'read:item'])]
    public function getAge()
    {
        $birthYear = $this->birthDate->format('Y');
        if (is_null($this->deathDate)) {
            $age = intval(DateTime::createFromFormat('Y', date('Y'))->format('Y')) - intval($birthYear);

            return $age;
        }
        else {
            $deathYear = $this->deathDate->format('Y');

            $age = intval($deathYear) - intval($birthYear);
            return $age;
        }
    }

    public function __toString()
    {
        return $this->name;
    }
}
